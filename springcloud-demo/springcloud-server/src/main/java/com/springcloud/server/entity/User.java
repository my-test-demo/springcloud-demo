package com.springcloud.server.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;

/**
 * @ClassName User
 * @Description TODO
 * @Author lyk
 * @Date 2018/9/18 14:34
 * @Version 1.0
 **/
@Data
@Entity
public class User {
    @Id
    @GeneratedValue
    private Long id;
    @Column
    private String userName;
    @Column
    private String name;
    @Column
    private String age;
    @Column
    private BigDecimal balance;
}
