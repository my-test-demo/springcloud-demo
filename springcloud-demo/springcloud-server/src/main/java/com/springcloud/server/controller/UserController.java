package com.springcloud.server.controller;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import com.springcloud.server.entity.User;
import com.springcloud.server.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * @ClassName UserController
 * @Description TODO
 * @Author lyk
 * @Date 2018/9/18 14:52
 * @Version 1.0
 **/
@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EurekaClient eurekaClient;

    @GetMapping("/eureka-instance")
    public String serviceUrl() {
        InstanceInfo instance = eurekaClient.getNextServerFromEureka("cloud-server", false);
        return instance.getHomePageUrl();
    }

    @Autowired
    private DiscoveryClient discoveryClient;
    @GetMapping("/eureka-info")
    public ServiceInstance showInfo(){
        ServiceInstance serviceInstance = discoveryClient.getLocalServiceInstance();
        return serviceInstance;
    }

    @RequestMapping(value = "/user/{id}",method = RequestMethod.GET)
    public User findUserById(@PathVariable Long id){
        return userRepository.findOne(id);
    }

    @PostMapping("/postUser")
    public User postUser(@RequestBody User user){
        user.setName("李四");
        user.setUserName("lisi");
        user.setAge("25");
        user.setBalance(BigDecimal.valueOf(120.00));

        return userRepository.save(user);
    }

    @GetMapping("getUser")
    public User getUser(User user){
        return user;
    }
}
