package com.springcloud.server.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import com.springcloud.server.SpringcloudServerApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.logging.Logger;

/**
 * @ClassName HelloController
 * @Description TODO
 * @Author lyk
 * @Date 2018/9/18 10:55
 * @Version 1.0
 **/
@RestController
@Slf4j
public class HelloController {

    private final Logger logger = Logger.getLogger(String.valueOf(SpringcloudServerApplication.class));

    @Autowired
    private DiscoveryClient client;

    @RequestMapping(value = "/hello",method = RequestMethod.GET)
    public String index() {
        ServiceInstance instance = client.getLocalServiceInstance();
//        log.info("/hello,host:"+instance.getHost()+";service_id:"+instance.getServiceId());
        logger.info("/hello,host:"+instance.getHost()+";service_id:"+instance.getServiceId());
        return "hello world";
    }
}
