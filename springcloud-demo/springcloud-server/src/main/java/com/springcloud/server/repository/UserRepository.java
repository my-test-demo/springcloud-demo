package com.springcloud.server.repository;

import com.springcloud.server.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @InterfaceName UserRepository
 * @Description TODO
 * @Author lyk
 * @Date 2018/9/18 14:46
 * @Version 1.0
 **/
@Repository
public interface UserRepository extends JpaRepository<User,Long> {

}
