package com.springcloud.feign.controller;

import com.springcloud.feign.feign.TestFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName TestController
 * @Description TODO
 * @Author lyk
 * @Date 2019/3/21 17:19
 * @Version 1.0
 **/
@RestController
public class TestController {
    @Autowired
    private TestFeign testFeign;

    @RequestMapping(value = "/hello",method = RequestMethod.GET)
    public String hello(){
        return testFeign.hello();
    }

    @RequestMapping(value = "/getServices")
    public Object getServices(){
        return testFeign.getServices();
    }

    @GetMapping(value = "/getWeather")
    public String getWeather(@RequestParam("cityName") String cityName){
        return testFeign.getWeather(cityName);
    }
}
