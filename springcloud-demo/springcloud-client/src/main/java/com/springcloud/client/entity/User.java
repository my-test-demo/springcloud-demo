package com.springcloud.client.entity;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @ClassName User
 * @Description TODO
 * @Author lyk
 * @Date 2018/9/18 14:34
 * @Version 1.0
 **/
@Data
public class User {
    private Long id;
    private String userName;
    private String name;
    private String age;
    private BigDecimal balance;
}
