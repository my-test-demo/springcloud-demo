package com.springcloud.client.controller;

import com.springcloud.client.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

/**
 * @ClassName TestController
 * @Description TODO
 * @Author lyk
 * @Date 2018/9/18 16:10
 * @Version 1.0
 **/
@RestController
public class TestController {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${user.userServicePath}")
    private String userServicePath;

    @RequestMapping(value = "/test/{id}",method = RequestMethod.GET)
    @ResponseBody
    public User findUserById(@PathVariable Long id){
        return restTemplate.getForObject(userServicePath + id,User.class);
    }
}
