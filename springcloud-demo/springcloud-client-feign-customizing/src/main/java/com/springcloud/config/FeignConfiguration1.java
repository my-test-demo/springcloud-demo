package com.springcloud.config;

import feign.Contract;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName FeignConfiguration1
 * @Description TODO
 * @Author lyk
 * @Date 2018/9/26 17:40
 * @Version 1.0
 **/
@Configuration
public class FeignConfiguration1 {
    @Bean
    public Contract feignContract(){
        return new Contract.Default();
    }
}
