package com.springcloud.client.feign;

import com.springcloud.client.entity.User;
import feign.Param;
import feign.RequestLine;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * @InterfaceName UserFeignClient
 * @Description TODO
 * @Author lyk
 * @Date 2018/9/21 10:58
 * @Version 1.0
 **/
@FeignClient("cloud-server")
public interface UserFeignClient {
    @RequestLine("GET/test/{id}")
    User findById(@Param("id") Long id);

}