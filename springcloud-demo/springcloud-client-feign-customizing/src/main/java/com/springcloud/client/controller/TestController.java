package com.springcloud.client.controller;

import com.springcloud.client.entity.User;
import com.springcloud.client.feign.UserFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName TestController
 * @Description TODO
 * @Author lyk
 * @Date 2018/9/18 16:10
 * @Version 1.0
 **/
@RestController
public class TestController {

    @Autowired
    private UserFeignClient userFeignClient;

////    @RequestMapping(value = "/test/{id}",method = RequestMethod.GET)
//    @GetMapping("/test/{id}")
//    public User findUserById(@PathVariable("id") Long id){
//        return userFeignClient.findUserById(id);
//    }
////    @RequestMapping(value = "/user",method = RequestMethod.GET)
//    @GetMapping("/postUser")
//    public User testPost(User user){
//        return userFeignClient.postUser(user);
//    }
//
//    @GetMapping("/getUser")
//    public User testGet(User user){
//        return userFeignClient.getUser(user);
//    }

}
