package com.springcloud.client.config;

import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName TestConfiguration
 * @Description TODO
 * @Author lyk
 * @Date 2018/9/20 13:19
 * @Version 1.0
 **/
@Configuration
@ExcludeFromComponentScan
public class TestConfiguration {

//    @Autowired
//    IClientConfig clientConfig;

    @Bean
    public IRule ribbonRule(){
        return new RandomRule();
    }
}
