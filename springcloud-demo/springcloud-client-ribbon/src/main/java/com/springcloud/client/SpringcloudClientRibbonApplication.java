package com.springcloud.client;

import com.springcloud.client.config.ExcludeFromComponentScan;
import com.springcloud.client.config.TestConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableDiscoveryClient
@RibbonClient(name = "cloud-server", configuration = TestConfiguration.class)
@ComponentScan(excludeFilters = {@ComponentScan.Filter(type = FilterType.ANNOTATION,value = ExcludeFromComponentScan.class)})
/*
 * TestConfiguration必须是@Configuration注解的配置类，但要注意主应用程序上下文不属于@ComponentScan，
 * 否则将由@RibbonClients所有者共享。如果您使用@ComponentScan（或@SpringBootApplication），
 * 则需要采取措施避免包含（例如将其放在一个单独的，不重叠的包中，或者指定要在@ComponentScan）。
 *
 */
public class SpringcloudClientRibbonApplication {
	@Bean
	//使其具有ribbon负载均衡的能力
	@LoadBalanced
	public RestTemplate restTemplate(){
		return new RestTemplate();
	}
	public static void main(String[] args) {
		SpringApplication.run(SpringcloudClientRibbonApplication.class, args);
	}
}
