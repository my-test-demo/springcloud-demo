package com.springcloud.client.controller;

import com.springcloud.client.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

/**
 * @ClassName UserController
 * @Description TODO
 * @Author lyk
 * @Date 2018/9/18 16:10
 * @Version 1.0
 **/
@RestController
public class UserController {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private LoadBalancerClient loadBalancerClient;

    @RequestMapping(value = "/user/{id}",method = RequestMethod.GET)
    @ResponseBody
    public User findUserById(@PathVariable Long id){
//        ServiceInstance serviceInstance = loadBalancerClient.choose("cloud-server");
//        System.out.println(serviceInstance.getServiceId()+":"+serviceInstance.getHost()+":"+serviceInstance.getPort());
        return restTemplate.getForObject("http://cloud-server/user/" + id,User.class);
    }

    @GetMapping("/test")
    public String test(){
        ServiceInstance serviceInstance = loadBalancerClient.choose("cloud-server");
        System.out.println("1:"+serviceInstance.getServiceId()+":"+serviceInstance.getHost()+":"+serviceInstance.getPort());

        ServiceInstance serviceInstance1 = loadBalancerClient.choose("cloud-client");
        System.out.println("2:"+serviceInstance1.getServiceId()+":"+serviceInstance1.getHost()+":"+serviceInstance1.getPort());
        return "test";
    }

    @GetMapping("/ribbon")
    public String ribbonTest(){
        return restTemplate.getForEntity("http://cloud-hello-server/hello",String.class).getBody();
    }
}
