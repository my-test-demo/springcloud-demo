package com.springcloud.client.feign;

import com.springcloud.client.entity.User;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * @InterfaceName UserFeignClient
 * @Description TODO
 * @Author lyk
 * @Date 2018/9/21 10:58
 * @Version 1.0
 **/
@FeignClient("cloud-server")
public interface UserFeignClient {

    //  1.不支持@GetMapping请求   2.必须使用@PathVariable设置value
    //    @GetMapping("/user/{id}")
    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    User findUserById(@PathVariable("id") Long id);

    //    @PostMapping("/postUser")
    @RequestMapping(value = "/postUser", method = RequestMethod.POST)
    User postUser(@RequestBody User user);

    //    @GetMapping("/getUser")
    // 该请求不会成功，只要参数是复杂对象，即使指定了是GET方式，feign依然会以POST方式发送请求。
    @RequestMapping(value = "/getUser", method = RequestMethod.GET)
    User getUser(@RequestBody User user);

}