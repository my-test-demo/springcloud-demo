package com.springcloud.consul.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.springcloud.consul.service.WeatherService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.URLEncoder;

/**
 * @ClassName WeatherServiceImpl
 * @Description TODO
 * @Author lyk
 * @Date 2019/9/16 16:36
 **/
@Service
@Slf4j
public class WeatherServiceImpl implements WeatherService {
    // "http://wthrcdn.etouch.cn/weather_mini"
//    private static final String WEATHER_URI = "http://t.weather.sojson.com/api/weather/city/";

    @Value("${weather.url}")
    private String weatherUrl;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public String getWeather(String cityName) {
        String url = String.format(weatherUrl, cityName, "UTF-8");
        String data = null;
        try {
            ResponseEntity<String> respString = restTemplate.getForEntity(url, String.class);
            String strBody = null;
            if (respString.getStatusCodeValue() == 200) {
                strBody = respString.getBody();
                JSONObject jsonObject = JSONObject.parseObject(strBody);
                data = jsonObject.getString("data");
                log.info("天气查询接口调用成功！");
            }
        } catch (RestClientException e) {
            log.info("天气查询接口调用失败,{}", e.getMessage());
        }
        return data;
    }
}
