package com.springcloud.consul.controller;


import com.ecwid.consul.v1.ConsulClient;
import com.ecwid.consul.v1.agent.model.Check;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * @ClassName TestController
 * @Description TODO
 * @Author lyk
 * @Date 2019/3/21 10:53
 * @Version 1.0
 **/
@RestController
@Slf4j
public class TestController {

    @Autowired
    private DiscoveryClient discoveryClient;

    @RequestMapping(value = "/hello",method = RequestMethod.GET)
    public String hello() {
        return "hello consul";
    }

    @RequestMapping(value = "/getServices")
    public Object getServices(){
        return discoveryClient.getServices();
    }

    @Autowired
    private ConsulClient consulClient;

    /**
     * 剔除所有无效的服务实例
     */
    @RequestMapping(value = "/CheakCleanService", method = RequestMethod.GET)
    public Map<String,String> CheakCleanService() {
        log.info("***********************开始清理consul上无效服务***********************");
        //获取所有的services检查信息
        Iterator<Map.Entry<String, Check>> it = consulClient.getAgentChecks().getValue().entrySet().iterator();
        Map.Entry<String, Check> serviceMap = null;
        Map<String,String> cleanMap = new HashMap<>();
        while (it.hasNext()) {
            //迭代数据
            serviceMap = it.next();
            //获取服务名称
            String serviceName = serviceMap.getValue().getServiceName();
            //获取服务ID
            String serviceId = serviceMap.getValue().getServiceId();
            log.info("服务名称 :{};服务ID:{}", serviceName, serviceId);
            //获取健康状态值  PASSING：正常  WARNING  CRITICAL  UNKNOWN：不正常
            log.info("服务 :{}的健康状态值：{}", serviceName, serviceMap.getValue().getStatus());
            if ( serviceMap.getValue().getStatus() == Check.CheckStatus.CRITICAL) {
                cleanMap.put(serviceName,serviceId);
                log.info("服务 :{}为无效服务，准备清理", serviceName);
                consulClient.agentServiceDeregister(serviceId);
            }
        }
        return cleanMap;
    }

}
